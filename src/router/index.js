import Vue from "vue";
import Router from "vue-router";

import PvpBracketStatistics from "../components/PvpBracketStatistics.vue";
import PvpLeaderboard from "../components/PvpLeaderboard.vue";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      name: "PvpBracketStatistics",
      component: PvpBracketStatistics
    },
    {
      path: "/pvpLeaderboards",
      name: "PvpLeaderboard",
      component: PvpLeaderboard
    }
  ]
});
